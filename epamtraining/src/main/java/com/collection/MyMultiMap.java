package com.collection;

import java.util.*;

public class MyMultiMap<K, V> implements MultiMap<K, V> {

    private Map<K, List<V>> backingMap = new HashMap<>();

    @Override
    public void put(K key, V value) {
        List<V> values = new ArrayList<>();
        if (backingMap.containsKey(key)) {
            values = this.backingMap.get(key);
            values.add(value);
        } else {
            values.add(value);
        }

        this.backingMap.put(key, values);
    }

    @Override
    public V get(K key) {
        return (V) backingMap.get(key);
    }

    @Override
    public int size() {
        return backingMap.size();
    }

    @Override
    public String toString() {
        return this.backingMap.toString();
    }
}
