package com.collection;

import java.util.Map;

public class Main {

    public static void main(String[] args) {
        MultiMap<String, Integer> multiMap = new MyMultiMap<>();

        multiMap.put("SÖR", 2);
        multiMap.put("SÖR", 3);
        multiMap.put("BOR", 5);

        System.out.println(multiMap);
    }
}
