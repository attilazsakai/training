package com.collection;

public interface MultiMap<K, V> {

    void put(K key, V value);

    V get(K key);

    int size();
}
